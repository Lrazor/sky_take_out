package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.anno.AutoFill;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryMapper {



    @AutoFill(OperationType.INSERT)
    @Insert("insert into category (type, name, sort, create_time, update_time, create_user, update_user, status) " +
            "values" +
            " (#{type}, #{name}, #{sort}, #{createTime}, #{updateTime}, #{createUser}, #{updateUser}, #{status})")
    void insert(Category category);

    Page<Category> pageQuery(CategoryPageQueryDTO categoryPageQueryDTO);

    @AutoFill(OperationType.UPDATE)
    void update(Category category);

    @AutoFill(OperationType.UPDATE)
    @Update("update category set status = #{status} where id = #{id}")
    void updatestatus(Category category);

    @Select("select * from category where type = #{type} order by sort asc")
    List<Category> list(Integer type);

    /**
     * 根据菜品id查询对应的分类id
     * @param id
     * @return
     */
    @Select("select id from dish where category_id=#{id}")
    Long getCategoryIdsByDishId(Long id);

    @Select("select id from setmeal where category_id=#{id}")
    Long getCategoryIdsBySetmealId(Long id);


    @Delete("delete from category where id=#{id}")
    void deleteById(Long id);
}
