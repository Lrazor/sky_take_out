package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.anno.AutoFill;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface EmployeeMapper {

    /**
     * 根据用户名查询员工
     * @param username
     * @return
     */
    @Select("select * from employee where username = #{username}")
    Employee getByUsername(String username);

    /**
     * 插入员工数据
     * @param employee
     */
    @AutoFill(OperationType.INSERT)
    @Insert("insert into employee (name, username, password, phone, sex, id_number, create_time, update_time, create_user, update_user, status)"  +
            "values" +
            "(#{name}, #{username}, #{password}, #{phone}, #{sex}, #{idNumber}, #{createTime}, #{updateTime}, #{createUser}, #{updateUser}, #{status})")
    void insert(Employee employee);

    //分页查询
    Page<Employee> pageQuery(EmployeePageQueryDTO employeePageQueryDTO);

    //更新员工启用禁用信息
    @AutoFill(OperationType.UPDATE)
    void update(Employee employee);

    //根据id查询员工
    @Select("select * from employee where id = #{id}")
    Employee getById(Long id);

    //更新员工信息
    @AutoFill(OperationType.UPDATE)
    @Update("update employee set username=#{username},name=#{name},phone=#{phone},sex=#{sex},id_number=#{idNumber} where id =#{id}")
    void updateEmployee(Employee employee);

    //更新密码
    @Update("update employee set password=#{password} where id =#{id}")
    void updatePassword(Employee employee);


}
