package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.anno.AutoFill;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface DishMapper {
    /**
     * 插入菜品数据
     */
    @AutoFill(OperationType.INSERT)
    void insert(Dish dish);

    Page<DishVO> PageQuery(DishPageQueryDTO dishPageQueryDTO);

    @Select("select * from dish where id = #{id}")
    Dish getById(Long id);

    @Delete("delete from dish where id = #{id}")
    void deleteById(Long id);


    @AutoFill(OperationType.UPDATE)
    void update(Dish dish);

    @AutoFill(OperationType.UPDATE)
    @Update("update dish set status = #{status} where id = #{id}")
    void updatestatus(Dish dish);

    @Select("select * from dish where category_id = #{categoryId} and status = 1")
    List<DishVO> queryDishByCategoryId(Long categoryId);


}
