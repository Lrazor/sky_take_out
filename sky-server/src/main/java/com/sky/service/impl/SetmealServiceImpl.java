package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
public class SetmealServiceImpl implements SetmealService {

    @Autowired
    private SetmealMapper setmealMapper;

    @Autowired
    private SetmealDishMapper setmealDishMapper;

    /**
     * 新增套餐 --套餐表 套餐菜品表
     * @param setmealDTO
     */
    @Override
    @Transactional
    public void saveWithDish(SetmealDTO setmealDTO) {
        //新增套餐 获取套餐基本信息--套餐表
        Setmeal setmeal = new Setmeal();//创建一个临时对象，用来接收前端传来新增的菜品信息
        BeanUtils.copyProperties(setmealDTO,setmeal);
        log.info("套餐基本信息：{}",setmeal);
        //插入套餐表
        setmealMapper.insert(setmeal);//将获取到的菜品信息插入到套餐表中 完成主键回显
        Long id = setmeal.getId();//获取到菜品表中的主键
        //套餐菜品关联表  插入菜品信息关联表中
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        log.info("套餐菜品关联表：{}",setmealDishes);

        // 遍历菜品信息，将菜品信息中的id设置为套餐id
        for (SetmealDish dish : setmealDishes) {
            dish.setSetmealId(id);
        }
        setmealDishMapper.insertBatch(setmealDishes);
        log.info("套餐菜品关联表插入数据：{}",setmealDishes);


    }

    /**
     * 分页查询套餐
     * @param setmealPageQueryDTO
     * @return
     */
    @Override
    public PageResult pageQuery(SetmealPageQueryDTO setmealPageQueryDTO) {
        PageHelper.startPage(setmealPageQueryDTO.getPage()
                             ,setmealPageQueryDTO.getPageSize());
        Page<SetmealVO> page = setmealMapper.pageQuery(setmealPageQueryDTO);
        PageResult pageResult = new PageResult(page.getTotal(),page.getResult());
        return pageResult;
    }

    /**
     * 批量删除套餐
     * @param ids
     */
    @Override
    @Transactional
    public void deleteBatchSetmeal(List<Long> ids) {
        //未选择套餐显示删除失败
        if (ids == null || ids.size()==StatusConstant.DISABLE){
            throw new DeletionNotAllowedException(MessageConstant.NOT_SELECTED);
        }
        //1:正在启售的套餐不能删除
        for (Long id : ids){
            //id 查询的套餐id
            //根据id查询相关菜品信息 解析菜品信息状态 查看是否是启售状态
            Setmeal setmeal = setmealMapper.getById(id);
            Integer status = setmeal.getStatus();
            if (status == StatusConstant.ENABLE){//1：启用 0：禁用
                throw new DeletionNotAllowedException(MessageConstant.SETMEAL_ON_SALE);
            }
        }
        //2:删除套餐同时删除套餐和菜品的关系
        for (Long id : ids){
            setmealMapper.deleteById(id);
            setmealDishMapper.deleteBySetmealId(id);
        }
    }

    @Override
    public void startOrStop(Integer status, Long id) {
        Setmeal setmeal = new Setmeal();
        setmeal.setId(id);
        setmeal.setStatus(status);
        setmealMapper.updatestatus(setmeal);
    }

    /**
     * 修改套餐信息及相关的套餐菜品信息
     * @param setmealDTO
     */
    @Override
    public void updateWithFlavor(SetmealDTO setmealDTO) {
        //setmealDTO包含了 要修改的套餐基本信息 id 和修改后的套餐菜品关联信息
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        //修改套餐基本信息
        setmealMapper.update(setmeal);

        //修改套餐菜品关联表
        //删除套餐菜品关联表中的菜品信息
        setmealDishMapper.deleteBySetmealId(setmeal.getId());
        //重新插入套餐菜品关联表中的菜品信息
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        for (SetmealDish setmealDish : setmealDishes){
            setmealDish.setSetmealId(setmeal.getId());
        }
        setmealDishMapper.insertBatch(setmealDishes);
    }

    @Override
    public SetmealVO getByIdWithFlavors(Long id) {
        SetmealVO setmealVO = new SetmealVO();
        log.info("根据id查询基本信息:{}",setmealVO);
        Setmeal setmeal = setmealMapper.getById(id);
        //将套餐信息拷贝到VO中
        BeanUtils.copyProperties(setmeal,setmealVO);
        //查询套餐菜品关联的菜品信息 封装到VO中
        //根据id查询套餐菜品关联表中的套餐菜品信息
        List<SetmealDish> setmealDishes = setmealDishMapper.getBysetmealId(id);
        setmealVO.setSetmealDishes(setmealDishes);
        return setmealVO;
    }
}
