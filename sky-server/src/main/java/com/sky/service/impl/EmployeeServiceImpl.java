package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.PasswordConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.entity.Employee;
import com.sky.exception.AccountLockedException;
import com.sky.exception.AccountNotFoundException;
import com.sky.exception.PasswordErrorException;
import com.sky.mapper.EmployeeMapper;
import com.sky.result.PageResult;
import com.sky.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import springfox.documentation.spi.service.contexts.SecurityContext;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 员工登录
     *
     * @param employeeLoginDTO
     * @return
     */
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();

        //1、根据用户名查询数据库中的数据
        Employee employee = employeeMapper.getByUsername(username);

        //2、处理各种异常情况（用户名不存在、密码不对、账号被锁定）
        if (employee == null) {
            //账号不存在
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        //密码比对
        // TODO 后期需要进行md5加密，然后再进行比对
        password = DigestUtils.md5DigestAsHex(password.getBytes());

        if (!password.equals(employee.getPassword())) {
            //密码错误
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }

        if (employee.getStatus() == StatusConstant.DISABLE) {
            //账号被锁定
            throw new AccountLockedException(MessageConstant.ACCOUNT_LOCKED);
        }

        //3、返回实体对象
        return employee;
    }

    @Override
    public void save(EmployeeDTO employeeDOT) {
        Employee employee = new Employee();
        //对象属性拷贝
        BeanUtils.copyProperties(employeeDOT, employee);

        //设置账户的状态，默认正常状态 1表示正常 0表示锁定
        employee.setStatus(StatusConstant.ENABLE);

        //设置密码，默认密码123456，进行md5加密处理
        employee.setPassword(DigestUtils.md5DigestAsHex(PasswordConstant.DEFAULT_PASSWORD.getBytes()));

//        //设置当前记录的创建时间和修改时间
//        employee.setCreateTime(LocalDateTime.now());
//        employee.setUpdateTime(LocalDateTime.now());
//
//        //设置当前记录人id和修改人id
//        employee.setCreateUser(BaseContext.getCurrentId());
//        employee.setUpdateUser(BaseContext.getCurrentId());

        employeeMapper.insert(employee);
    }

    //分页查询
    @Override
    public PageResult pageQuery(EmployeePageQueryDTO employeePageQueryDTO) {
        // 使用PageHelper插件进行分页查询的准备工作
        // 该方法会根据传入的页码和页面大小参数，设置分页条件，以便后续的查询操作能够按照分页条件执行
        // 参数1: 页码，表示当前查询的是第几页数据
        // 参数2: 页面大小，表示每页数据的数量
        PageHelper.startPage(employeePageQueryDTO.getPage(), employeePageQueryDTO.getPageSize());
        // 使用EmployeePageQueryDTO作为查询条件，从employeeMapper中分页查询员工信息
        Page<Employee> page = employeeMapper.pageQuery(employeePageQueryDTO);

        // 获取当前分页查询的总记录数
        long total = page.getTotal();

        // 获取分页结果中的员工记录列表
        List<Employee> records = page.getResult();

        return new PageResult(total,records);
    }

    @Override
    public void startOrStop(Integer status, Long id){
        Employee employee = new Employee();
        employee.setStatus(status);
        employee.setId(id);
//        employee.setUpdateTime(LocalDateTime.now());
//        employee.setUpdateUser(BaseContext.getCurrentId());
        employeeMapper.update(employee);
    }

    /**
     * 根据id查询员工
     *
     * @param id
     * @return
     */
    @Override
    public Employee getById(Long id) {
        Employee employee = employeeMapper.getById(id);
        employee.setPassword("******");
        return employee;
    }

    @Override
    public void update(EmployeeDTO employeeDOT) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeDOT, employee);
//        employee.setUpdateTime(LocalDateTime.now());
//        employee.setUpdateUser(BaseContext.getCurrentId());
        employeeMapper.updateEmployee(employee);
    }

    @Override
    public void updatePassword(PasswordEditDTO passwordEditDTO) {
        log.info("修改密码：{}", passwordEditDTO);
        // 根据当前登录用户的id查询员工数据
        Long empId = BaseContext.getCurrentId();
        log.info("当前登录用户的id为：{}", empId);
        // 根据id查询数据库中的密码
        Employee employee = employeeMapper.getById(empId);
        String password = employee.getPassword();
        log.info("数据库中的密码为：{}", password);
        // 获取输入的旧密码
        String oldPassword = passwordEditDTO.getOldPassword();
        log.info("输入的旧密码为：{}", oldPassword);
        // 用输入的旧密码和数据库中密码比较判断输入的旧密码是否正确
        if (!password.equals(DigestUtils.md5DigestAsHex(oldPassword.getBytes()))) {
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }
        // 获取输入的新密码
        String newPassword = passwordEditDTO.getNewPassword();
        log.info("输入的新密码为：{}", newPassword);
        employee.setPassword(DigestUtils.md5DigestAsHex(newPassword.getBytes()));
        employeeMapper.updatePassword(employee);
    }

}
