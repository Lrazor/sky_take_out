package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.*;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private EmployeeServiceImpl employeeServiceImpl;
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;


    /**
     * 新增分类
     *
     * @param categoryDTO
     */
    @Override
    public void save(CategoryDTO categoryDTO) {
        //创建一个分类对象 用来接收数据
        Category category = new Category();
        //将dto中从前端页面接收过来的数据赋值给category对象
        BeanUtils.copyProperties(categoryDTO, category);//把接收来的数据拷贝给category
        //分类状态默认为禁用状态0
        category.setStatus(StatusConstant.DISABLE);
        //设置创建时间、更新时间、创建人、更新人
//        category.setCreateTime(LocalDateTime.now());
//        category.setUpdateTime(LocalDateTime.now());
//
//        Long currentId = BaseContext.getCurrentId();
//        category.setCreateUser(currentId);//创建人
//        category.setUpdateUser(currentId);//更新人
        categoryMapper.insert(category);
    }

    /**
     * 分类分页查询
     *
     * @param categoryPageQueryDTO
     * @return
     */

    @Override
    public PageResult pageQuery(CategoryPageQueryDTO categoryPageQueryDTO) {
        //开启分页 用Mybatis分页插件 getPage()方法 第几页 getPageSize()方法 每页显示多少条
        PageHelper.startPage(
                categoryPageQueryDTO.getPage(),
                categoryPageQueryDTO.getPageSize()
        );
        // 调用mapper层 将从前端获取的剩余数据 封装到Page集合中
        Page<Category> page = categoryMapper.pageQuery(categoryPageQueryDTO);
        // 将查询到的总页数数据和当前页的结果列表数据 封装到pageResult中
        PageResult pageResult = new PageResult(page.getTotal(), page.getResult());
        // 返回数据
        return pageResult;
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        // TODO 查询当前分类是否关联了菜品，如果关联了就抛出业务异常
        Long dishId = categoryMapper.getCategoryIdsByDishId(id);
        //根据菜品id 判断是否关联菜品
        if (dishId!= null && dishId > 0){
            throw new DeletionNotAllowedException(MessageConstant.CATEGORY_BE_RELATED_BY_DISH);
        }
        // TODO 查询当前分类是否关联了套餐，如果关联了就抛出业务异常
        Long setmealId = categoryMapper.getCategoryIdsBySetmealId(id);
        if (setmealId!= null && setmealId > 0){
            throw new DeletionNotAllowedException(MessageConstant.CATEGORY_BE_RELATED_BY_SETMEAL);
        }
        categoryMapper.deleteById(id);//删除当前分类信息

    }

    @Override
    public void update(CategoryDTO categoryDTO) {
        Category category = new Category();
        BeanUtils.copyProperties(categoryDTO,category);
//        category.setUpdateTime(LocalDateTime.now());
//        category.setUpdateUser(BaseContext.getCurrentId());
        categoryMapper.update(category);
    }

    @Override
    public void startOrStop(Integer status, Long id) {
        //查询分类下的菜品是否在启售，如果在启售则抛出异常
        Category category = new Category();
        category.setId(id);
        category.setStatus(status);
//        category.setUpdateTime(LocalDateTime.now());
//        category.setUpdateUser(BaseContext.getCurrentId());
        categoryMapper.updatestatus(category);
    }

    @Override
    public List<Category> list(Integer type) {
        List<Category> list = categoryMapper.list(type);
        log.info("list:{}",list);
            return list;
    }
}
