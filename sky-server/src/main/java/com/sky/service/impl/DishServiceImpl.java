package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.DishFlavorMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.netty.util.internal.MacAddressUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.ListResourceBundle;

@Service
@Slf4j
public class DishServiceImpl implements DishService {
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private DishFlavorMapper dishFlavorMapper;
    @Autowired
    private SetmealDishMapper setmealDishMapper;


    @Override
    @Transactional//事务管理 要么同时成功，要么同时失败
    public void saveWithFlavor(DishDTO dishDTO) {

        log.info("新增菜品，菜品数据存到dish表和口味表中：{}", dishDTO);
        //创建一个dish对象
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
                                //copy 对应的属性！！
        //添加到dish表
        dishMapper.insert(dish);//主键回显

        //添加口味到口味表
        //获取菜品id
        Long id = dish.getId();
        //获取口味集合
        List<DishFlavor> flavors = dishDTO.getFlavors();//flavors是口味信息
        //加口味表数据之前 将菜品id设置进去
        if (flavors != null && flavors.size()>0){//有值才遍历
            flavors.forEach(dishFlavor -> dishFlavor.setDishId(id));
            //加入口味表
            dishFlavorMapper.insertBatch(flavors);//批量插入
        }

    }

    @Override
    public PageResult pageQuery(DishPageQueryDTO dishPageQueryDTO) {
        PageHelper.startPage(dishPageQueryDTO.getPage(),
                             dishPageQueryDTO.getPageSize());
        Page<DishVO> page = dishMapper.PageQuery(dishPageQueryDTO);
        PageResult pageResult = new PageResult(page.getTotal(),page.getResult());
        return pageResult;
    }

    @Override
    @Transactional
    public void deleteBatch(List<Long> ids) {
        //删除菜品

        //1：正在启售的菜品不能删除
        for (Long id : ids){
            //id 查询的菜品id
            //根据id查询相关菜品信息 解析菜品信息状态 查看是否是启售状态
            Dish dish = dishMapper.getById(id);
            Integer status = dish.getStatus();
            if (status == StatusConstant.ENABLE){//1：启用 0：禁用
                throw new DeletionNotAllowedException(MessageConstant.DISH_ON_SALE);
            }
        }
        //2：被套餐关联的菜品不能删除
        List<Long> setmealIds = setmealDishMapper.getSetmealIdsByDishIds(ids);
        //根据菜品ids 判断是否关联套餐
        if (setmealIds!= null && setmealIds.size()>0){//有关联套餐
            throw new DeletionNotAllowedException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }
        //3:删除菜品同时删除菜品口味表中数据
        //根据菜品id删除 菜品口味表中 当前菜品对应多个口味
        for (Long id : ids){//id 菜品id
            dishMapper.deleteById(id);//删除当前菜品信息
            dishFlavorMapper.deleteByDishId(id);//删除当前菜品对应的口味信息
        }
    }

    @Override
    public DishVO getByIdWithFlavors(Long id) {
        //准备好接收数据的对象
        DishVO dishVO = new DishVO();
        //查询两个信息，菜品信息和菜品口味信息
        Dish dish = dishMapper.getById(id);
        List<DishFlavor> dishFlavors = dishFlavorMapper.getByDishId(id);
        //复制菜品信息到dishVO对象中
        BeanUtils.copyProperties(dish,dishVO);
        dishVO.setFlavors(dishFlavors);
        return dishVO;
    }

    /**
     * 根据id修改菜品
     * 修改菜品同时修改口味
     * @param dishDTO
     */
    @Override
    @Transactional
    public void updateWithFlavor(DishDTO dishDTO) {
        //根据id修改菜品基本信息
        //获取新的对象用来封装需要修改的菜品基本信息
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        //修改菜品
        dishMapper.update(dish);//根据id修改对应的数据

        Long dishId = dishDTO.getId();
        //删除口味表中的数据
        dishFlavorMapper.deleteByDishId(dishId);
        //添加新的口味数据
        List<DishFlavor> flavors = dishDTO.getFlavors();
            flavors.forEach(dishFlavor -> dishFlavor.setDishId(dishId));
            for (DishFlavor flavor : flavors){
                flavor.setDishId(dishId);
            }
            //插入到 关联的表中
            dishFlavorMapper.insertBatch(flavors);


    }

    /**
     * 菜品的启售停售
     * @param status
     * @param id
     */
    @Override
    public void startOrStop(Integer status, Long id) {
        Dish dish = new Dish();
        dish.setId(id);
        dish.setStatus(status);
        dishMapper.updatestatus(dish);
    }

    @Override
    public List<DishVO> queryDishByCategoryId(Long categoryId) {
        List<DishVO> list =dishMapper.queryDishByCategoryId(categoryId);
        return list;
    }

}
