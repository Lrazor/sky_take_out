package com.sky.service;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.vo.SetmealVO;

import java.util.List;

public interface SetmealService {

    void saveWithDish(SetmealDTO setmealDTO);

    PageResult pageQuery(SetmealPageQueryDTO setmealPageQueryDTO);

    void deleteBatchSetmeal(List<Long> ids);

    void startOrStop(Integer status, Long id);

    void updateWithFlavor(SetmealDTO setmealDTO);

    SetmealVO getByIdWithFlavors(Long id);
}
