package com.sky.aop;


import com.sky.anno.AutoFill;
import com.sky.context.BaseContext;
import com.sky.enumeration.OperationType;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;


import java.lang.reflect.Method;
import java.time.LocalDateTime;

@Component//被spring发现
@Aspect//切面
@Slf4j//日志
public class AutoFillAspect {
    /*
    配置切面  -- 对谁在哪进行怎么样的增强
     */
    @Before("@annotation(com.sky.anno.AutoFill)")
    public void autoFill(JoinPoint joinPoint) {
        //joinPoint  被增强的原始方法
        //1：基于joinPoint获取原始方法Method对象形式
        //1.1:获取方法签名
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        //1.2:获取到原始方法Method对象
        Method method = signature.getMethod();
        //2:解析 method上的注解
        AutoFill autoFill = method.getAnnotation(AutoFill.class);
        OperationType operationType = autoFill.value();
        log.info("自动填充操作:{},操作类型:{}", method, operationType);

        //3:获取到 方法的参数对象
        Object[] args = joinPoint.getArgs();
        if (args == null || args.length == 0){
            log.info("没有参数，无法完成自动填充");
            return;
        }
        Object entity = args[0];
        //装备自动填充的数据值
        Long currentId = BaseContext.getCurrentId();
        LocalDateTime now = LocalDateTime.now();
        if (operationType == OperationType.INSERT){
            //新增 --对方法的参数对象 设置四个属性 --完成四个属性自动填充
            try {
                log.info("做的是新增的自动填充");
//                category.setCreateTime(LocalDateTime.now());
                Method setCreateTime = entity.getClass().getDeclaredMethod("setCreateTime", LocalDateTime.class);
                setCreateTime.invoke(entity,now);
//                category.setUpdateTime(LocalDateTime.now());
                Method setUpdateTime = entity.getClass().getDeclaredMethod("setUpdateTime", LocalDateTime.class);
                setUpdateTime.invoke(entity,now);
//                Long currentId = BaseContext.getCurrentId();
//                category.setCreateUser(currentId);//创建人
                Method setCreateUser = entity.getClass().getDeclaredMethod("setCreateUser", Long.class);
                setCreateUser.invoke(entity,currentId);
//                category.setUpdateUser(currentId);//更新人
                Method setUpdateUser = entity.getClass().getDeclaredMethod("setUpdateUser", Long.class);
                setUpdateUser.invoke(entity,currentId);
            }catch (Exception e){
                log.info("没有找到对应的set方法");
                e.printStackTrace();
            }
        }else {
            //更新 --对方法的参数对象 设置两个属性--完成两个属性自动填充
            try{
                log.info("做的是更新的自动填充");
//                category.setUpdateTime(LocalDateTime.now());
                Method setUpdateTime = entity.getClass().getDeclaredMethod("setUpdateTime", LocalDateTime.class);
                setUpdateTime.invoke(entity,now);
//                category.setUpdateUser(BaseContext.getCurrentId());
                Method setUpdateUser = entity.getClass().getDeclaredMethod("setUpdateUser", Long.class);
                setUpdateUser.invoke(entity,currentId);
            }catch (Exception e){
                log.info("没有找到对应的set方法");
                e.printStackTrace();
            }
        }
    }
}
