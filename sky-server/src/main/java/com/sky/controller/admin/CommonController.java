package com.sky.controller.admin;


import com.sky.properties.AliyunOSSProperties;
import com.sky.result.Result;
import com.sky.utils.AliyunOSSUtils;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.nio.channels.MulticastChannel;

@RestController//交给spring管理
@RequestMapping("/admin/common")
@Slf4j
@Api(tags = "通用接口")
public class CommonController {
    @Autowired
    private AliyunOSSProperties aliyunOSSProperties;

    /**
     * 实现文件上传
     */
    @PostMapping("/upload")

    public Result<String> upload(MultipartFile file) throws Exception {
        log.info("文件开始上传，文件名为:{}",file);
        //将file 上传到阿里云oss中
        //获取字节数组
        byte[] content = file.getBytes();
        //获取文件后缀名
        String originalFilename = file.getOriginalFilename();
        String extName = originalFilename.substring(originalFilename.lastIndexOf("."));
        //调用工具类的方法
        String url = AliyunOSSUtils.upload(aliyunOSSProperties.getEndpoint(),aliyunOSSProperties.getBucketName(),content,extName);
        return Result.success(url);
    }
}
