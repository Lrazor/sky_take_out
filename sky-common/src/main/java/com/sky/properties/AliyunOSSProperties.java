package com.sky.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

//这里面存储阿里云相关的属性集
@Data
@Component
@ConfigurationProperties(prefix = "sky.alioss")
public class AliyunOSSProperties {

    private String endpoint ;

    private String bucketName ;
}
